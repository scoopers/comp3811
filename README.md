# comp3811

VC for graphics module  

* Made on ubuntu linux.

* Depends on  
  * qt-5  
  * qmake3  
  * opengl 2.1+  
  * glut  

* Controls  
  * WSAD for movement  
  * Space to jump  

* Sometimes it starts at a strange angle, just move the world angle slider to 0.  
* Click the window to allow movement with keys.
